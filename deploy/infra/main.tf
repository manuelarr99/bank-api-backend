terraform {

  backend "s3" {
    bucket = "bank-api-backend-devops-tfstate"
    key = "bank-api-backend.tfstate"
    region = "us-east-2"
    encrypt = true
    dynamodb_table = "bank-api-backend-devops-tfstate-lock"
  }
  
}

provider "aws" {
    region = "us-east-2"
    version = "~> 2.54.0"
}
